import java.util.Arrays;

/**
 * LeftRotation
 */
public class LeftRotation {

    public static void main(String[] args) {
        int[] a = { 1, 2, 3, 4, 5 };
        int d = 4;
        System.out.println(Arrays.toString(rotLeft(a, d)));
 
    }

  
    static int[] rotLeft(int a[], int d) {
        int n  = a.length;
        for (int i = 0; i < d; i++)
            leftRotatebyOne(a, n);
        return a;
    }

    static void leftRotatebyOne(int a[], int n) {
        int temp;
        for (int i = 1; i <= a.length - 1; i++) {
            temp = a[i];
            a[i] = a[i - 1];
            a[i - 1] = temp;

        }
    }
}