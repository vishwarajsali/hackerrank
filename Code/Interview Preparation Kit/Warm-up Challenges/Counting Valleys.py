#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the countingValleys function below.
def countingValleys(n, s):
    level = 0
    valleys = 0
    belowsea = False
    for i in s:
        level += (1 if i == 'U' else -1)
        if not belowsea and level < 0:
            valleys += 1
            belowsea = True
        if level >= 0:
            belowsea = False

    return( valleys)
        

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    s = input()

    result = countingValleys(n, s)

    fptr.write(str(result) + '\n')

    fptr.close()
