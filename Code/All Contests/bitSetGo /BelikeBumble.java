import java.util.Arrays;
import java.util.Scanner;

/**
 * Be like Bumble
 */
public class BelikeBumble {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        while(t > 0) {
            int c = scanner.nextInt(); 
            long n = scanner.nextInt();
            long r=(long) (c * (long)Math.pow(n, 3));
            System.out.println(r);
        }
        scanner.close();
    }
}