N = int(input())
x = list(map(int, input().rstrip().split()))

import math
def Mean(x):
    N = len(x)
    sumArr = 0
    for i in x:
        sumArr += i
    mean = sumArr/ N
    return mean

def StandardDeviation(x):
    lenX = len(x)
    mid = Mean(x)
    sum =0
    for i in x:
        sum += pow((i-mid), 2)
    sd = math.sqrt(sum/lenX)
    return round(sd, 1)

print(StandardDeviation(x))