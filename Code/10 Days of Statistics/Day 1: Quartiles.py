def Median(values, start_index, stop_index):
    n = stop_index - start_index + 1
    m = start_index + n // 2
    if (n % 2) == 0:
        return (values[m - 1] + values[m]) / 2
    else:
        return values[m]


def Quartiles(values):
    values = sorted(values)
    length = len(values)
    Q2 = Median(values, 0, length - 1)
    m = length // 2
    if (length % 2) == 0:
        Q1 = Median(values, 0, m - 1)
        Q3 = Median(values, m , length - 1)
    else:
        Q1 = Median(values, 0, m - 1)
        Q3 = Median(values, m + 1, length - 1)
    return Q1, Q2, Q3


n = int(input())
x = list(map(int, input().rstrip().split()))
result = map('{:g}'.format, Quartiles(x))
print(*result, sep="\n")