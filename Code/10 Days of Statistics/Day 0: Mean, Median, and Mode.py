N = int(input())
x = list(map(int, input().rstrip().split()))

#Mean
def Mean(x):
    N = len(x)
    sumArr = 0
    for i in x:
        sumArr += i
    mean = sumArr/ N
    return mean

#Median
def Median(x):
    N = len(x)
    x = sorted(x)
    mid = (N-1)//2
    if N % 2 == 0:
        return (x[mid] + x[mid+1])/2
    else:
        return x[mid]

#Mode
def Mode(x):
    mode= 0
    lenX = len(x)
    count, maxOcc = 0,0
    
    sortedX = x
    sortedX.sort()
    
    current = 0
    for i in sortedX:
        if (i == current):
            count += 1
        else:
            count = 1
            current = i
        if (count > maxOcc):
            maxOcc = count
            mode = i
    return mode
    
print(Mean(x))
print(Median(x))
print(Mode(x))

