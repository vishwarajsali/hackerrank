N = int(input())
x = list(map(int, input().rstrip().split()))
w = list(map(int, input().rstrip().split()))

def WeightedMean(x,w):
    WeightedMean, xI, wI = 0, 0,0
    lenX = len(x)
    for i in range(lenX):
        xI += x[i] * w[i]
        wI += w[i]
    WeightedMean = xI/ wI
    return(round(WeightedMean,1))

print(WeightedMean(x,w))
