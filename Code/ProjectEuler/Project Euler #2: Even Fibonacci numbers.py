#!/bin/python3

import sys

def sum_even_fibonacci(n):
    fn_1, fn_2,sum = 1,1,0
    while True:
        fn = fn_1 + fn_2
        if fn >= n: return sum
        if fn % 2 == 0 : sum+= fn
        fn_2,fn_1 = fn_1, fn

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    print(sum_even_fibonacci(n))