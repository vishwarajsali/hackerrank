def countingValleys(s):
    climb = False

    count = 0
    for i in range(len(s)-1):
        if s[i] != s[i+1]:
            count+=1
    return count

s = 'DDUUUDDU'
print(countingValleys(s))