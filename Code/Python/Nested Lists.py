if __name__ == '__main__':
    students = list()
    for _ in range(int(input())):
        name = input()
        score = float(input())
        students.append([name, score])
        
    scores = set([students[name][1] for name in range(len(students))])
    scores = list(scores)
    scores.sort()
    
    students = [x[0] for x in students if x[1] == scores[1]]
    students.sort()
    
    for student in students:
        print(student)
    