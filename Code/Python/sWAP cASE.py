def swap_case(s):
    r =""
    for char in s:
        if char.isupper():
            r += char.lower()
        else :
            r += char.upper()
    return r