if __name__ == '__main__':
    N = int(input())  
    List = []
    for _ in range(N):
        commands = input().split()
        command = commands[0]
        argument = commands[1:]
        if command != 'print':
            command += "("+",".join(argument)+")"
            eval("List."+command)
        else:
            print(List)