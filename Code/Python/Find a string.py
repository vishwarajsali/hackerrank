def count_substring(string, sub_string):
    lenSub_string = len(sub_string)
    count = 0
    for i in range(len(string) - lenSub_string+1):
        sub = string[i: i+lenSub_string]
        if sub_string == sub: 
            count+=1
    return count