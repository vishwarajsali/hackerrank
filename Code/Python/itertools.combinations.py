# Enter your code here. Read input from STDIN. Print output to STDOUT


from itertools import combinations

A, B = input().split()

for x in range (1,int(B)+1):
    for i in combinations(sorted(A),x):
        print (''.join(i))