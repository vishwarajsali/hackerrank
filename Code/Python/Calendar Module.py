# Enter your code here. Read input from STDIN. Print output to STDOUT

import calendar

day = list(map(int, input().split()))
if day[2] > 2000 and day[2] < 3000:
    print(list(calendar.day_name)[calendar.weekday(day[2], day[0], day[1])].upper())
