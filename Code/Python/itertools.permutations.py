# Enter your code here. Read input from STDIN. Print output to STDOUT

from itertools import permutations

A, B = input().split()

for i in sorted(permutations(A, int(B))):
    print (''.join(i))