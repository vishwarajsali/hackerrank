if __name__ == '__main__':
    n = int(input())
    student_marks = {}
    for _ in range(n):
        name, *line = input().split()
        scores = list(map(float, line))
        student_marks[name] = scores
    query_name = input()
    if query_name in student_marks:
        query_scores = student_marks[query_name]
        avg = (query_scores[0]+query_scores[1]+query_scores[2])/3
        print("{:.2f}".format(avg))
   
# if __name__ == '__main__':
#     n = int(input())
#     student_marks = {}
#     for _ in range(n):
#         name, *line = input().split()
#         scores = list(map(float, line))
#         student_marks[name] = scores
#     query_name = input()
#     query_scores = student_marks[query_name]
#     print("{0:.2f}".format(sum(query_scores)/3))