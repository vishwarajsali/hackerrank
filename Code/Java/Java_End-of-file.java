import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /*
         * Enter your code here. Read input from STDIN. Print output to STDOUT. Your
         * class should be named Solution.
         */

        // use stdin
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        // scanner.hasnext()
        while (scanner.hasNext()) {
            count++;
            // use stdin
            String out = scanner.nextLine();
            // use stdout
            System.out.println(count + " " + out);
        }
        scanner.close();
    }
}