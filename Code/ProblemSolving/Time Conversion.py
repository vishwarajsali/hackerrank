s = '12:40:22PM'

pm = True if s[-2:] == 'PM' else False
is12 = True if s[:2] == '12' else False

if not pm and is12:
    hours = '00'
elif not pm: 
    hours = s[:2]
elif pm and is12:
    hours = s[:2]
else:
    hours = str(int(s[:2])+12)


print(hours+s[2:-2])