package Practice.Interview_Preparation_Kit.Arrays;

/**
 * NewYearChaos
 */
public class NewYearChaos {

    public static void main(String[] args) {
        int[] q = { 2, 5, 1, 3, 4 };
        minimumBribes(q);
    }

    static void minimumBribes(int[] q) {
        int bribes = 0;
        for (int i = q.length-1; i>= 0;i-- ) {
            if((q[i] - (i+1)) > 2) {
                System.out.println("Too chaotic");
                return;
            }

            for(int j = Math.max(0, q[i]-2); j< i; j++){
                if(q[j]> q[i])bribes++;
            }
        }

        System.out.println(bribes);

    }
}