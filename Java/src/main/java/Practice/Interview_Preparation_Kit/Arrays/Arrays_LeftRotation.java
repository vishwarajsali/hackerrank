package Practice.Interview_Preparation_Kit.Arrays;

import java.util.Arrays;

/**
 * Arrays:_LeftRotation
 */
public class Arrays_LeftRotation {

    public static void main(String[] args) {
        int[] a  = {1, 2, 3, 4 ,5};
        int d = 4;
        System.out.println(Arrays.toString(rotLeft(a, d)));
    }

    static int[] rotLeft(int[] a, int d) {
        
           for(int i = 0; i < d; i++) rotLeftByOne(a);

        return a;
    }

    private static void rotLeftByOne(int[]a){
        int temp = a[0], i;
        for( i = 0; i< a.length-1; i++) a[i] = a[i+1];
        a[i] = temp;

    }

    static int[] rotate(int[]a){

        int i = 0;
        int first = a[0];
        for(; i< a.length-1; i++){
            a[i] = a[i+1];
        }
        a[i] = first;

        return a;
    }

    
}