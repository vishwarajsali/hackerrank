package Practice.Interview_Preparation_Kit.Arrays;

/**
 * _2DArray_DS
 */
public class _2DArray_DS {

    public static void main(String[] args) {
        int[][] array = { { 1, 1, 1, 0, 0, 0 }, { 0, 1, 0, 0, 0, 0 }, { 1, 1, 1, 0, 0, 0 }, { 0, 0, 2, 4, 4, 0 },
                { 0, 0, 0, 2, 0, 0 }, { 0, 0, 1, 2, 4, 0 } };

        System.out.println(hourglassSum(array));
    }

    static int hourglassSum(int[][] arr) {

        int max = Integer.MIN_VALUE;
        for(int i = 0; i< arr.length-2; i++){
            for(int j = 0; j < arr.length-2; j++){
                int sum = arr[i][j] + arr[i][j+1] + arr[i][j+2] + arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1]+ arr[i+2][j+2];
                //System.out.printf("%d %d %d \n  %d \n%d %d %d \n", arr[i][j], arr[i][j+1], arr[i][j+2], arr[i+1][j+1], arr[i+2][j], arr[i+2][j+1], arr[i+2][j+2]);
                max = Math.max(max, sum);
            }
        }
        return max == Integer.MIN_VALUE ? -1 : max;
    }

}