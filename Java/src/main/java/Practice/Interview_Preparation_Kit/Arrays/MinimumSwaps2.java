package Practice.Interview_Preparation_Kit.Arrays;

/**
 * MinimumSwaps2
 */
public class MinimumSwaps2 {

    public static void main(String[] args) {
        int[] arr = {7, 1, 3, 2, 4, 5, 6};
        System.out.println(minimumSwaps(arr));
    }

    static int minimumSwaps(int[] arr) {
        int swap = 0;

        int i = 0;
        while(i < arr.length-1){
            
            if(arr[i]!= (i+1)){
                int j = i+1;
                while(j < arr.length){
                    if(arr[j] == (i+1)){
                        int temp = arr[i];
                        arr[i] = arr[j];
                        arr[j] = temp;
                        swap++;
                        break;
                    }
                    j++;
                }
            }
            
            i++;
        }
        return swap;

    }
}